const app = getApp()

Page({
  data: {
    reasonType: "请选择原因",
    reportReasonArray: app.reportReasonArray,
    publishUserId: "",
    videoId: ""
  },

  onLoad: function(params) {
    var me = this;

    var videoInfo = wx.getStorageSync("videoInfo");
    var videoId = videoInfo.id;
    var publishUserId = videoInfo.userId;

    me.setData({
      publishUserId: publishUserId,
      videoId: videoId,
    });
  },

  changeMe: function(e) {
    var me = this;

    var index = e.detail.value;
    var reasonType = app.reportReasonArray[index];

    me.setData({
      reasonType: reasonType
    });
  },

  submitReport: function(e) {
    wx.showLoading({
      title: '举报中',
    })
    var me = this;

    var reasonIndex = e.detail.value.reasonIndex;
    var reasonContent = e.detail.value.reasonContent;
    if (reasonIndex == null || reasonIndex == '' || reasonIndex == undefined) {
      wx.showToast({
        title: '选择举报理由',
        icon: "none"
      })
      return;
    }

    var serverUrl = app.serverUrl;
    var userInfo = wx.getStorageSync("userInfo");
    console.log(app.reportReasonArray[reasonIndex]);
    wx.request({
      url: serverUrl + 'user/report',
      method: 'get',
      data: {
        publishUserId: me.data.publishUserId,
        videoId: me.data.videoId,
        title: app.reportReasonArray[reasonIndex] + "",
        content: reasonContent,
        userId: userInfo.id
      },
      header: {
        'content-type': 'application/json', // 默认值
        'headerUserId': userInfo.id,
        'headerUserToken': userInfo.userToken
      },
      success: function(res) {
        wx.hideLoading();
        wx.showToast({
          title: res.data.data,
          duration: 2000,
          icon: 'none',
          success: function() {

          }
        })
        setTimeout(function() {
          wx.navigateBack();
        }, 2000);
      }

    })

  }

})