const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (params) {
    console.log(params);
    if (params.videoId != null || params.videoId != undefined) {
      console.log('11111' + params.videoId);
      var serverUrl = app.serverUrl;
      wx.request({
        url: serverUrl + 'video/queryById?videoId=' + params.videoId,
        method: 'get',
        success: function (res) {
          console.log(res.data.data);
          wx.setStorageSync("videoInfo", res.data.data);

          wx.redirectTo({
            url: '../videoinfo/videoinfo',
          })
        }
      })
    }
    else {
      wx.showToast({
        title: '视频不存在',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})