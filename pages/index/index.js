const app = getApp()

Page({
  data: {
    // 用于分页的属性
    totalPage: 1,
    page: 1,
    videoList: [],

    screenWidth: 350,
    serverUrl: "",

    searchContent: ""
  },

  onLoad: function(params) {
    var me = this;
    var screenWidth = wx.getSystemInfoSync().screenWidth;
    me.setData({
      screenWidth: screenWidth,
    });
    // 获取当前的分页数
    var page = me.data.page;

    var keyword = params.keyword;
    console.log(keyword);
    if (keyword != null && keyword != undefined) {
      me.getAllVideoList(page, keyword);
    } else {

      me.getAllVideoList(page);
    }
  },

  getAllVideoList: function(page, keyword) {
    var me = this;
    var serverUrl = app.serverUrl;
    wx.showLoading({
      title: '请等待，加载中...',
    });
    var url = serverUrl + 'video/list?page=' + page;
    if (keyword != null && keyword != undefined) {
      url = serverUrl + 'video/list?page=' + page + '&keyword=' + keyword;
    }


    wx.request({
      url: url,
      method: "get",
      success: function(res) {
        wx.hideLoading();
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();

        console.log(res.data);

        // 判断当前页page是否是第一页，如果是第一页，那么设置videoList为空
        if (page === 1) {
          me.setData({
            videoList: []
          });
        }

        var videoList = res.data.data.row;
        var newVideoList = me.data.videoList;

        
        if(videoList.length==0)
        {
          wx.showModal({
            title: '提示',
            content: '当前没有搜索的内容可能不存在',
            success: function (res) {
              if (res.confirm) {
                me.getAllVideoList(1, null);
              } else {
                me.getAllVideoList(1, null);
              }
            }
          })
        }

        me.setData({
          videoList: newVideoList.concat(videoList),
          page: page,
          totalPage: res.data.data.total,
        });

      }
    })
  },

  onPullDownRefresh: function() {
    wx.showNavigationBarLoading();
    this.getAllVideoList(1, null);
  },

  onReachBottom: function() {
    var me = this;
    var currentPage = me.data.page;
    var totalPage = me.data.totalPage;

    // 判断当前页数和总页数是否相等，如果想的则无需查询
    if (currentPage === totalPage) {
      wx.showToast({
        title: '已经没有视频啦~~',
        icon: "none"
      })
      return;
    }

    var page = currentPage + 1;

    me.getAllVideoList(page, null);
  },

  showVideoInfo: function(e) {
    var me = this;
    var videoList = me.data.videoList;
    var arrindex = e.target.dataset.arrindex;
    console.log(videoList[arrindex]);
    wx.setStorageSync("videoInfo", videoList[arrindex]);

    wx.redirectTo({
      url: '../videoinfo/videoinfo'
    })
  }

})