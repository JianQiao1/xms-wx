const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    faceUrl: "../resource/images/noneface.png",
    isMe: true,
    videoSelClass: "video-info",
    isSelectedWork: "video-info-selected",
    isSelectedLike: "",
    isSelectedFollow: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var user = app.getGlobalUserInfo();
    var that = this;
    var me = this;
    var userId = user.id;
    var serverUrl = app.serverUrl;
    // 调用后端

    var I = options.isMe;
    if (I != null && I != undefined) { //展示他人信息
      wx.setStorageSync("I", false);
      //设置个人信息
      var publisher = wx.getStorageSync("publisher");
      wx.request({
        url: serverUrl + 'user/isFollow?userId=' + publisher.id + '&fansId=' + user.id,
        method: 'get',
        header: {
          'headerUserId': user.id,
          'headerUserToken': user.userToken
        },
        success: function(res) {
          if (res.data.data.follow) {
            that.setData({
              isFollow: true,
              followMsg: '已关注',
              fansCounts: res.data.data.userFansCounts
            })
            if (res.data.data.myFans) {
              that.setData({
                isFollow: true,
                followMsg: '互相关注',
                fansCounts: res.data.data.userFansCounts,
              })
              wx.setStorageSync('myFans', true);
            }
            if (res.data.data.mutualFollow) {
              that.setData({
                isFollow: true,
                followMsg: '互相关注',
                fansCounts: res.data.data.userFansCounts
              })
            }
          } else {
            that.setData({
              isFollow: false,
              fansCounts: res.data.data.userFansCounts
            })
          }
        }
      })
      me.setData({
        faceUrl: publisher.faceImg,
        fansCounts: publisher.fansCounts,
        followCounts: publisher.followCounts,
        receiveLikeCounts: publisher.receiveLikeCounts,
        nickname: publisher.nickName,
        isMe: false
      });

    } else {
      wx.setStorageSync("I", true);
      wx.showLoading({
        title: '请等待...',
      });
      wx.request({
        url: serverUrl + 'user/info?userId=' + userId,
        method: "get",
        header: {
          'content-type': 'application/json', // 默认值
          'headerUserId': user.id,
          'headerUserToken': user.userToken
        },
        success: function(res) {
          wx.hideLoading();
          if (res.data.status == 200) {
            var userInfo = res.data.data;
            var faceUrl = "../resource/images/noneface.png";
            if (userInfo.faceImg != null && userInfo.faceImg != '' && userInfo.faceImg != undefined) {
              faceUrl = userInfo.faceImg;
            }

            me.setData({
              faceUrl: faceUrl,
              fansCounts: userInfo.fansCounts,
              followCounts: userInfo.followCounts,
              receiveLikeCounts: userInfo.receiveLikeCounts,
              nickname: userInfo.nickName,
              isMe: true
            });
          } else {
            wx.showToast({
              title: res.data.msg,
              duration: 3000,
              icon: "none",
              success: function() {
                wx.redirectTo({
                  url: '../userLogin/login',
                })
              }
            })
          }
        }
      })
      wx.removeStorageSync("isMe");
    }
    this.doSelectWork();

  },
  logout: function() {
    wx.showLoading({
      title: '正在注销会话',
    })
    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + 'logout',
      method: 'post',
      data: {
        'userId': user.id,
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.status == 200) {
          app.setGlobalUserInfo(null);
          wx.showToast({
            title: '退出成功',
            icon: 'success',
            duration: 3000
          });
          setTimeout(function() {
            wx.redirectTo({
              url: '../userLogin/login',
            })
          }, 2000)

        }
      }
    })
  },
  changeFace: function(e) {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album'],
      success: function(res) {
        wx.showLoading({
          title: '正在上传...',
        })
        var tempFilePath = res.tempFilePaths;
        var serverUrl = app.serverUrl;
        var user = app.getGlobalUserInfo();
        wx.uploadFile({
          url: serverUrl + '/user/uploadFace?userId=' + user.id,
          filePath: tempFilePath[0],
          header: {
            'headerUserId': user.id,
            'headerUserToken': user.userToken
          },
          name: 'file',
          success: function(res) {
            wx.hideLoading();
            var data = JSON.parse(res.data);
            if (data.status == 200) {
              wx.showToast({
                title: '上传成功',
                icon: 'success',
                duration: 3000
              });
              //设置返回的图片为头像
              that.setData({
                faceUrl: data.data
              })
            } else {
              wx.showToast({
                title: data.msg,
                duration: 3000
              });
            }
          }
        })
      },
    })
  },
  uploadVideo: function() {
    wx.showLoading({
      title: '正在上传...',
    })
    var that = this;
    wx.chooseVideo({
      sourceType: ['album'],
      success: function(res) {
        var duration = res.duration;
        var height = res.height;
        var width = res.width;
        var size = res.size;
        var tempFilePath = res.tempFilePath;
        var thumbTempFilePath = res.thumbTempFilePath;

        wx.hideLoading();
        if (duration > 181 || duration < 5) {
          wx.showToast({
            title: '视频长度为5秒到3分钟',
            icon: "none",
            duration: 2500
          })
        } else {
          wx.showToast({
            title: '上传成功',
            icon: "success",
            duration: 2500
          })
          //打开bgm页面
          wx.navigateTo({
            url: '../chooseBgm/chooseBgm?duration=' + duration +
              "&width=" + width +
              "&height=" + height +
              "&tempFilePath=" + tempFilePath +
              "&thumbTempFilePath=" + thumbTempFilePath
          })
        }
      }
    })
  },
  followMe: function(e) {
    var that = this;
    var serverUrl = app.serverUrl;
    var user = app.getGlobalUserInfo();

    var publisher = wx.getStorageSync("publisher");
    var followType = e.currentTarget.dataset.followtype;

    if (followType == '1') {
      var url = serverUrl + 'user/follow?userId=' + publisher.id + '&fansId=' + user.id;

    } else if (followType == '0') {
      var url = serverUrl + 'user/unFollow?userId=' + publisher.id + '&fansId=' + user.id;

    }
    wx.request({
      url: url,
      method: 'get',
      header: {
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function(res) {

        if (followType == "1") {
          that.setData({
            isFollow: true,
            fansCounts: that.data.fansCounts + 1
          })

          var isMyFans = wx.getStorageSync('myFans');
          if (isMyFans) {
            that.setData({
              followMsg: '互相关注',
            })
          } else {
            that.setData({
              followMsg: '已关注',
            })
          }

        } else {
          that.setData({
            isFollow: false,
            fansCounts: that.data.fansCounts - 1
          })
        }
        publisher.fansCounts = that.data.fansCounts
        wx.setStorageSync("publisher", publisher);
        wx.showToast({
          title: res.data.data,
        })
      }

    })

  },
  onUnload: function() {
    wx.removeStorageSync("myFans");
    console.log("移除一个页面缓存 myFans");
  },
  doSelectWork: function() {
    this.setData({
      isSelectedWork: "video-info-selected",
      isSelectedLike: "",
      isSelectedFollow: "",

      myWorkFalg: false,
      myLikesFalg: true,
      myFollowFalg: true,

      myVideoList: [],
      myVideoPage: 1,
      myVideoTotal: 1,

      likeVideoList: [],
      likeVideoPage: 1,
      likeVideoTotal: 1,

      iFollow: [],
      iFollowPage: 1,
      iFollowTotal: 1
    });
    //展示视频
    var I = wx.getStorageSync("I");
    console.log(I);
    var user = app.getGlobalUserInfo();
    var publisher = wx.getStorageSync("publisher");
    if (I) {
      this.getMyVideoList(user.id, 1);
    } else {
      this.getMyVideoList(publisher.id, 1);
    }
  },
  getMyVideoList: function(userId, page) {
    var that = this;
    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + 'video/listById?userId=' + userId + '&page=' + page + '&pageSize=6',
      method: 'get',
      header: {
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function(res) {
        console.log(res.data.data);
        var myVideoList = res.data.data.row;
        var newVideoList = that.data.myVideoList;
        that.setData({
          myVideoList: newVideoList.concat(myVideoList),
          myVideoTotal: res.data.data.total,
          myVideoPage: res.data.data.page,
          myVideoTotal: res.data.data.total
        })
      }
    })
  },
  doSelectLike: function() {
    this.setData({
      isSelectedWork: "",
      isSelectedLike: "video-info-selected",
      isSelectedFollow: "",

      myWorkFalg: true,
      myLikesFalg: false,
      myFollowFalg: true,

      myVideoList: [],
      myVideoPage: 1,
      myVideoTotal: 1,

      likeVideoList: [],
      likeVideoPage: 1,
      likeVideoTotal: 1,

      iFollow: [],
      iFollowPage: 1,
      iFollowTotal: 1
    });
    //展示视频
    var I = wx.getStorageSync("I");
    console.log(I);
    var user = app.getGlobalUserInfo();
    var publisher = wx.getStorageSync("publisher");
    if (I) {
      this.getMyLikeList(user.id, 1);
    } else {
      this.getMyLikeList(publisher.id, 1);
    }
  },
  getMyLikeList: function(userId, page) {
    var that = this;
    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + 'video/myLikeById?userId=' + userId + '&page=' + page + '&pageSize=6',
      method: 'get',
      header: {
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function(res) {
        console.log(res);
        var likeVideoList = res.data.data.row;
        var newLikeVideoList = that.data.likeVideoList;
        that.setData({
          likeVideoList: newLikeVideoList.concat(likeVideoList),
          likeVideoTotal: res.data.data.total,
          likeVideoPage: res.data.data.page,
        })
      }
    })
  },
  doSelectFollow: function() {
    this.setData({
      isSelectedWork: "",
      isSelectedLike: "",
      isSelectedFollow: "video-info-selected",

      myWorkFalg: true,
      myLikesFalg: true,
      myFollowFalg: false,

      myVideoList: [],
      myVideoPage: 1,
      myVideoTotal: 1,

      likeVideoList: [],
      likeVideoPage: 1,
      likeVideoTotal: 1,

      iFollow: [],
      iFollowPage: 1,
      iFollowTotal: 1
    })
    var I = wx.getStorageSync("I");
    console.log(I);
    var user = app.getGlobalUserInfo();
    var publisher = wx.getStorageSync("publisher");
    if (I) {
      this.getFollow(user.id, 1);
    } else {
      this.getFollow(publisher.id, 1);
    }

  },
  getFollow: function(userId, page) {
    var that = this;
    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + 'user/myFollowUser?userId=' + userId + '&page=' + page + '&pageSize=6',
      method: 'get',
      header: {
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function(res) {
        console.log(res);
        var iFollow = res.data.data.row;
        var newIFollow = that.data.iFollow;
        console.log(that.data);
        that.setData({
          iFollow: newIFollow.concat(iFollow),
          iFollowTotal: res.data.data.total,
          iFollowPage: res.data.data.page,
        })
      }
    })
  },
  showVideo: function(e) {
    console.log(e);

    var myWorkFalg = this.data.myWorkFalg;
    var myLikesFalg = this.data.myLikesFalg;
    var myFollowFalg = this.data.myFollowFalg;

    if (!myWorkFalg) {
      var videoList = this.data.myVideoList;
    } else if (!myLikesFalg) {
      var videoList = this.data.likeVideoList;
    } else if (!myFollowFalg) {
      var videoList = this.data.followVideoList;
    }

    var arrindex = e.target.dataset.arrindex;
    console.log(videoList[arrindex]);

    var videoInfo = videoList[arrindex];
    wx.setStorageSync("videoInfo", videoInfo);
    wx.redirectTo({
      url: '../videoinfo/videoinfo',
    })
  },
  onReachBottom: function() {
    var myWorkFalg = this.data.myWorkFalg;
    var myLikesFalg = this.data.myLikesFalg;
    var myFollowFalg = this.data.myFollowFalg;

    if (!myWorkFalg) {
      var that = this;
      //展示视频
      var I = wx.getStorageSync("I");
      console.log(I);
      var user = app.getGlobalUserInfo();
      var publisher = wx.getStorageSync("publisher");
      var page = that.data.myVideoPage + 1;
      var total = that.data.myVideoTotal;
      if (page <= total) {
        if (I) {
          this.getMyVideoList(user.id, page);
        } else {
          this.getMyVideoList(publisher.id, page);
        }
      } else {
        wx.showToast({
          title: '到底了',
        })
      }

    } else if (!myLikesFalg) {
      var that = this;
      //展示视频
      var I = wx.getStorageSync("I");
      console.log(I);
      var user = app.getGlobalUserInfo();
      var publisher = wx.getStorageSync("publisher");
      var page = that.data.iFollowPage + 1;
      var total = that.data.iFollowTotal;

      if (page <= total) {
        if (I) {
          this.getFollow(user.id, page);
        } else {
          this.getFollow(publisher.id, page);
        }
      } else {
        wx.showToast({
          title: '到底了',
        })
      }
    } else if (!myFollowFalg) {
      var that = this;
      //展示视频
      var I = wx.getStorageSync("I");
      console.log(I);
      var user = app.getGlobalUserInfo();
      var publisher = wx.getStorageSync("publisher");
      var page = that.data.likeVideoPage + 1;
      var total = that.data.likeVideoTotal;

    }
  },
  myFollow: function(e) {
    var userId = e.currentTarget.dataset.arrindex;
    console.log(userId);
    var that = this;
    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + 'user/info?userId=' + userId,
      method: 'get',
      header: {
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function(res) {
        console.log(res);
        var publisher = res.data.data;
        wx.setStorageSync("publisher", publisher);
        if(user.id==publisher.id)
        {
          wx.setStorageSync("I", true);
          wx.navigateTo({
            url: '../mine/mine',
          })
        }else
        {
          wx.setStorageSync("I", false);
          wx.navigateTo({
            url: '../mine/mine?isMe=false',
          })
        }
      }
    })
  }
})