const app = getApp()

Page({
  data: {
    mode: "scaleToFill",
    arr: [],
    indicatorDots: true,
    autoplay: true,
    interval: 3000,
    duration: 1000,
  },
  onLoad:function()
  {
    var that = this;
    //从服务器获取图片，然后存到数组里
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + 'sys/swiper',
      method: 'get',
      success: function (res) {
        that.setData({ arr: res.data.data })
      }
    })
  },
  doRegist: function(e) {
    wx.showLoading({
      title: '正在注册...',
    })
    var formObj = e.detail.value;
    var username = formObj.username;
    var password = formObj.password;
    var rePassword = formObj.rePassword;
    var email = formObj.email;

    if (username.length == 0) {
      wx.showToast({
        title: '用户名不能为空',
        icon: 'none',
        duration: 3000
      })
      return;
    } else if (username.length < 2 || username.length > 10) {
      wx.showToast({
        title: '用户名的长度为2-10位',
        icon: 'none',
        duration: 3000
      })
      return;
    }
    if (email.length == 0) {
      wx.showToast({
        title: '邮箱不能为空',
        icon: 'none',
        duration: 3000
      })
      return;
    } else if (!(/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/.test(email))) {
      wx.showToast({
        title: '邮箱的正确格式为xxxx@xx.xx',
        icon: 'none',
        duration: 3000
      })
      return;
    }

    if (password.length == 0) {
      wx.showToast({
        title: '密码不能为空',
        icon: 'none',
        duration: 3000
      })
      return;
    } else if (password.length <= 6 || password.length > 18) {
      wx.showToast({
        title: '密码的长度为6-18位',
        icon: 'none',
        duration: 3000
      })
      return;
    }

    if (password!=rePassword)
    {
      wx.showToast({
        title: '两次输入的密码不一样',
        icon: 'none',
        duration: 3000
      })
      return;
    }

    console.log("用户名:" + username + ",密码:" + password + ",邮箱:" + email)
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + '/register',
      method: 'post',
      data: {
        'username': username,
        'password': password,
        'email': email,

      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.status == 200) {
          console.log(res.data)
          wx.showToast({
            title: '邮件发送成功',
            icon: 'success',
            duration: 3000
          })
          setTimeout(function() {
            wx.redirectTo({
              url: '../userLogin/login',
            })
          }, 2000)

        } else {
          wx.showModal({
            title: '!',
            content: res.data.msg,
            success: function(res) {
              if (res.confirm) {
                setTimeout(function () {
                  wx.redirectTo({
                    url: '../userLogin/login',
                  })
                }, 2000)
              }
            }
          })
        }
      }
    })
  },
  goLoginPage: function() {
    wx.redirectTo({
      url: '../userLogin/login',
    })
  }
})