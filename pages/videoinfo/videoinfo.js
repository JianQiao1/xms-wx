var videoUtil = require('../../utils/videoUtil.js')
var userUtil = require('../../utils/userUtil.js')
const app = getApp()

Page({
  data: {
    cover: "cover",
    videoId: "",
    src: "",
    videoInfo: {},

    userLikeVideo: false,


    commentsPage: 1,
    commentsTotalPage: 1,
    commentsList: [],


    placeholder: "说点什么..."
  },

  videoCtx: {},
  onLoad: function() {
    var me = this;
    var serverUrl = app.serverUrl;
    me.videoCtx = wx.createVideoContext("myVideo", me);
    var user = app.getGlobalUserInfo();

    // 获取上一个页面传入的参数
    var videoInfo = wx.getStorageSync("videoInfo");
    wx.removeStorageSync("videoInfo");
    var height = videoInfo.videoHeight;
    var width = videoInfo.videoWidth;
    var cover = "cover";
    if (width >= height) {
      cover = "";
    }

    if (user != null && user != undefined) {
      var url = serverUrl + 'user/queryPublisher?userId=' + user.id + '&videoId=' + videoInfo.id + '&publisherId=' + videoInfo.userId;
    } else
      var url = serverUrl + 'user/queryPublisher?videoId=' + videoInfo.id + '&publisherId=' + videoInfo.userId;


    wx.request({
      url: url,
      method: 'get',
      success: function(res) {
        console.log(res);
        me.setData({
          publisher: res.data.data.users,
          userLikeVideo: res.data.data.userLikeVideo
        });
        console.log(res.data.data.users);
      }
    })

    me.setData({
      videoId: videoInfo.id,
      src: videoInfo.videoPath,
      videoInfo: videoInfo,
      cover: cover
    });

    me.getCommentsList(1);
  },

  onShow: function() {
    var me = this;
    me.videoCtx.play();
  },

  onHide: function() {
    var me = this;
    me.videoCtx.pause();
  },

  showSearch: function() {
    wx.navigateTo({
      url: '../searchVideo/searchVideo',
    })
  },

  upload: function() {
    var that = this;
    var user = app.getGlobalUserInfo();
    var videoInfo = that.data.videoInfo;
    userUtil.isLogin(null, videoInfo);
    videoUtil.uploadVideo();
  },

  showIndex: function() {
    wx.redirectTo({
      url: '../index/index',
    })
  },


  showMine: function() {
    var that = this;
    var user = app.getGlobalUserInfo();
    var videoInfo = that.data.videoInfo;
    userUtil.isLogin('../mine/mine', videoInfo);
  },

  likeVideoOrNot: function() {
    var me = this;
    var that = this;
    var serverUrl = app.serverUrl;
    var user = app.getGlobalUserInfo();
    var videoInfo = that.data.videoInfo;
    userUtil.isLogin(null, videoInfo);
    var userLikeVideo = me.data.userLikeVideo;
    var url = serverUrl + '/video/userLike?userId=' + user.id + '&videoId=' + videoInfo.id + '&videoCreateId=' + videoInfo.userId;
    if (userLikeVideo == true) {
      var url = serverUrl + '/video/userUnLike?userId=' + user.id + '&videoId=' + videoInfo.id + '&videoCreateId=' + videoInfo.userId;
    }
    wx.request({
      url: url,
      method: 'get',
      header: {
        'userId': user.id,
        'userToken': user.userToken
      },
      success: function(res) {
        console.log(res);
        me.setData({
          userLikeVideo: !userLikeVideo
        })
      }
    })

  },
  showPublisher: function() {
    var that = this;
    var user = app.getGlobalUserInfo();
    var videoInfo = that.data.videoInfo;
    var publisher = that.data.publisher;
    wx.setStorageSync("publisher", publisher);
    if (publisher.id == user.id) {
      userUtil.isLogin('../mine/mine', videoInfo);
    } else {
      userUtil.isLogin('../mine/mine?isMe=false', videoInfo);
    }
  },

  shareMe: function() {
    var me = this;
    var user = app.getGlobalUserInfo();

    wx.showActionSheet({
      itemList: ['举报用户', '分享到朋友圈', '分享到QQ空间', '分享到微博'],
      success: function(res) {
        console.log(res.tapIndex);
        if (res.tapIndex == 0) {
          // 举报
          var videoInfo = me.data.videoInfo;
          var user = app.getGlobalUserInfo();
          console.log(user);
          wx.setStorageSync("videoInfo", videoInfo); //举报成功以后回传视频页面
          // userUtil.isLogin('../userLogin/login', videoInfo);
          if (user != null && user != undefined) {
            wx.navigateTo({
              url: '../report/report'
            })
          } else {
            wx.redirectTo({
              url: '../userLogin/login',
            })
          }

        } else {
          wx.showToast({
            title: '官方暂未开放...',
          })
        }
      }
    })
  },

  onShareAppMessage: function(res) {
    var that = this;
    var videoInfo = that.data.videoInfo;
    var vid = videoInfo.id;
    console.log(vid);
    return {
      title: '邀请你一起来看',
      path: 'pages/share/share?videoId=' + vid
    }
  },

  leaveComment: function() {
    this.setData({
      commentFocus: true
    });
  },

  replyFocus: function(e) {
    var fatherCommentId = e.currentTarget.dataset.fathercommentid;
    var toUserId = e.currentTarget.dataset.touserid;
    var toNickname = e.currentTarget.dataset.tonickname;

    this.setData({
      placeholder: "回复  " + toNickname,
      replyFatherCommentId: fatherCommentId,
      replyToUserId: toUserId,
      commentFocus: true
    });
  },

  saveComment:function(e) {
    var me = this;
    var content = e.detail.value;

    // 获取评论回复的fatherCommentId和toUserId
    var fatherCommentId = e.currentTarget.dataset.replyfathercommentid;
    var toUserId = e.currentTarget.dataset.replytouserid;
    var videoInfo=me.data.videoInfo;
    var user = app.getGlobalUserInfo();
    wx.setStorageSync("videoInfo", videoInfo);
    if (user == null || user == undefined || user == '') {
      wx.navigateTo({
        url: '../userLogin/login',
      })
    } else {
      wx.showLoading({
        title: '请稍后...',
      })
      wx.request({
        url: app.serverUrl + 'comment?fromUserId=' + user.id
          + '&videoId=' + me.data.videoInfo.id
          + '&comment=' +content,
        method: 'POST',
        header: {
          'content-type': 'application/json', // 默认值
          'headerUserId': user.id,
          'headerUserToken': user.userToken
        },
        success: function(res) {
          console.log(res)
          console.log(me.data.videoInfo.id);
          wx.hideLoading();

          me.setData({
            contentValue: "",
            commentsList: []
          });

          me.getCommentsList(1);
        }
      })
    }
  },

  commentsPage: 1,
    commentsTotalPage: 1,
    commentsList: [],

  getCommentsList: function(page) {
    var me = this;

    var videoId = me.data.videoInfo.id;

    wx.request({
      url: app.serverUrl + '/comment?videoId=' + videoId + "&page=" + page + "&pageSize=5",
      method: "get",
      success: function(res) {
        console.log(res.data);

        var commentsList = res.data.data.row;
        var newCommentsList = me.data.commentsList;

        me.setData({
          commentsList: newCommentsList.concat(commentsList),
          commentsPage: page,
          commentsTotalPage: res.data.data.total
        });
      }
    })
  },

  onReachBottom: function() {
    var me = this;
    var currentPage = me.data.commentsPage;
    var totalPage = me.data.commentsTotalPage;
    if (currentPage === totalPage) {
      return;
    }
    var page = currentPage + 1;
    me.getCommentsList(page);
  }
})