const app = getApp()

Page({
  data: {
    mode: "scaleToFill",
    arr: [],
    indicatorDots: true,
    autoplay: true,
    interval: 3000,
    duration: 1000,
  },
  onLoad()
  {
    var that=this;
    //从服务器获取图片，然后存到数组里
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl+'sys/swiper',
      method:'get',
      success:function(res)
      {
        that.setData({ arr: res.data.data})
      }
    })
  }
  ,
  doLogin: function(e) {
    var that=this;
    var formObj = e.detail.value;
    var username = formObj.username;
    var password = formObj.password;

    if (username.length == 0) {
      wx.showToast({
        title: '用户名不能为空',
        icon: 'none',
        duration: 3000
      })
      return;
    } else if (username.length < 2 || username.length > 10) {
      wx.showToast({
        title: '用户名的长度为2-10位',
        icon: 'none',
        duration: 3000
      })
      return;
    }
    if (username.length == 0) {
      wx.showToast({
        title: '用户名不能为空',
        icon: 'none',
        duration: 3000
      })
      return;
    } else if (username.length < 2 || username.length > 10) {
      wx.showToast({
        title: '用户名的长度为2-10位',
        icon: 'none',
        duration: 3000
      })
      return;
    }
    wx.showLoading({
      title: '正在登陆...',
    })
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + '/login',
      method: 'post',
      data: {
        'username': username,
        'password': password,
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res) {
        wx.hideLoading();
        console.log(res);
        if (res.data.status == 200) {
          //app.userInfo=res.data.data;
          // fixme 修改原有的全局对象为本地缓存
          app.setGlobalUserInfo(res.data.data);
          var videoInfo=wx.getStorageSync("videoInfo");
          if (videoInfo != null && videoInfo!=''&&videoInfo!=undefined)
          {
            wx.redirectTo({
              url: '../videoinfo/videoinfo',
            })
          }else
          {
            wx.redirectTo({
              url: '../mine/mine',
            })
          }
        
        } else {
          wx.showModal({
            title: '!',
            content: res.data.msg,
            success: function(res) {
              if (res.confirm) {
                setTimeout(function() {
                  wx.redirectTo({
                    url: '../userLogin/login',
                  })
                }, 2000)
              }
            }
          })
        }

      }

    })
  },
  goRegistPage: function() {
    wx.redirectTo({
      url: '../userRegist/regist',
    })
  }
})