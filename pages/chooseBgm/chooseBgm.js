const app = getApp()

Page({
  data: {
    bgmList: [],
    videoParams: {}
  },

  onLoad: function(params) {
    var user = app.getGlobalUserInfo();
    var that = this;
    that.setData({
      videoParams: params
    });
    var serverUrl = app.serverUrl;
    console.log(user);
    wx.request({
      url: serverUrl + 'sys/music',
      method: 'get',
      header:{
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function(res) {
        console.log(res);
        that.setData({
          bgmList: res.data.data
        })
      }
    })
  },
  upload: function(e) {
    wx.showLoading({
      title: '正在上传...',
    })
    var that = this;
    var bgmId = e.detail.value.bgmId;
    var desc = e.detail.value.desc;

    var duration = that.data.videoParams.duration;
    var width = that.data.videoParams.width;
    var height = that.data.videoParams.height;
    var tempFilePath = that.data.videoParams.tempFilePath;
    var thumbTempFilePath = that.data.videoParams.thumbTempFilePath;

    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    console.log(tempFilePath);
    wx.uploadFile({
      url: serverUrl + 'video/upload',
      header: {
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      formData: {
        'userId': user.id,
        'videoSeconds': duration,
        'videoWidth': width,
        'videoHeight': height,
        'videoPath': tempFilePath,
        'videoDesc': desc,
        'audioId': bgmId
      },
      filePath: tempFilePath,
      name: 'file',
      success: function(res) {
        wx.hideLoading();
        var data = JSON.parse(res.data);
        console.log(data);
        var videoId = data.data;
        if (data.status == 200) {
          wx.showToast({
            title: '上传成功',
            icon: 'success',
            duration: 3000
          });
          wx.redirectTo({
            url: '../mine/mine',
          })
        } else {
          wx.showToast({
            title: data.msg,
            duration: 3000
          });
        }
      }
    })
  },




  // },
  // closeOther:function(e)
  // {
  //   var that=this;
  //   //获取当前播放实例。并且将其他bgm设置成停止播放状态
  //   var list = that.data.bgmList;

  //   var palyId = e.target.id;
  //   var musicList=[];
  //   for (var item in list)
  //   {
  //     if (list[item].id != palyId)
  //     {
  //       musicList.push(list[item].id);
  //     }
  //   }

  //   console.log("没播放的id"+musicList);
  //   //获取当前所有的歌曲，将当前正在播放的实例的id取出
  //   //将所有歌曲的id里去掉正在播放的id，挨个关闭剩下的歌曲



  // }
})