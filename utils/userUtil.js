function isLogin(url,videoInfo) {
  var that = this;
  var user = wx.getStorageSync("userInfo");
  console.log(user);
  if (user == null || user == undefined || user == '') {
    wx.setStorageSync("videoInfo", videoInfo);
    wx.showModal({
      title: '提示',
      content: '此账户登陆信息已过期，请重新登陆',
      success: function (res) {
        if (res.confirm) {
          if(url!=null)
          {
            wx.navigateTo({
              url: '../userLogin/login',
            })
          }
        } else {
        }
      }
    })
  } else {
    wx.navigateTo({
      url: url,
    })
  }
}

module.exports = {
  isLogin: isLogin
}
